This project has been completed in a virtualbox ubuntu environment using C++, Opengl and QT.
The frame rates and speeds have been configured to work properly on HP Spectre laptop with
Intel i7 processor, 16GB Ram, 1TB SSD and NVIDIA GeForce GTX 1050 graphics card. Actual performance
on other machines may vary. The application has been tested on school machines and should work as normal.
Note, the graphics in the application on a 4k display are really good but unfortunately, on some
school machines that have low resolution monitors, the graphics are rendered poorly. The window
dimensions have been set in Main.cpp so that it works properly on school machines. For a high
resolution display, this can be changed by commenting out the appropriate line in the file.

Build Instructions:
    1: Please ensure you have QT 5 installed then run qmake command to generate a makefile.
    2: then run make command to generate an executable file with the name najm
    3: then run the executable, in linux environments this is done as ./najm

How to use:
    click somewhere on the floor to begin!
    How long you click determines the how far the ball will go
    press r to reset ball and have another go

    Use the left and right arrow keys to manually modify joint angles
    Use 1,2,3 or 4 to set which joint is being manipulated
    Use w,a,s,d to move camera around, q to bring camera down, e to lift camera up
