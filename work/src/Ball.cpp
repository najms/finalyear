#include "Ball.h"

/*-----------------------------------------------------------------------------
            Class constructor, sets the balls initial position.
-----------------------------------------------------------------------------*/
Ball::Ball(float x1, float y1, float z1){
    position.x=x1;
    position.y=y1;
    position.z=z1;
}

/*-----------------------------------------------------------------------------
   Decrements the y value in the velocity vector by a small gravity constant.
   Uses new velocity vector to update balls position.
-----------------------------------------------------------------------------*/
void Ball::move() {
    if(active==true){
        velocity.y=velocity.y-gravity;

        position.x+=velocity.x*speed;
        position.y+=velocity.y*speed;
        position.z+=velocity.z*speed;
    }
}


/*-----------------------------------------------------------------------------
    Renders a small sphere on the screen which required temporarily disabling
    GL_TEXTURE_2D.
-----------------------------------------------------------------------------*/
void Ball::draw(bool translate){
    glDisable(GL_TEXTURE_2D);

    glPushMatrix();
    if(translate==true){
        glTranslatef( position.x, position.y, position.z );
    }
    glutSolidSphere( 0.4, 20, 20 );
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);
}
