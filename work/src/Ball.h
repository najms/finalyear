#include <QGLWidget>
#include <GL/glut.h>
#include <math.h>
#include <QtGui>
#include "Robot.h"

/*-----------------------------------------------------------------------------
    Header file and class declaration for Ball.cpp. This class renders a small
    sphere representing a ball using the draw function. The move function is
    used to update the balls position so that it can be thrown from one point to another.
-----------------------------------------------------------------------------*/
class Ball{
    public:
        Ball(float x1, float y1, float z1);
        void move();
        void draw(bool translate);

        Robot::Vector3 position, velocity;
        bool active=true;
        float gravity=0.01, speed=0.4;
};
