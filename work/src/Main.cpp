#include <QApplication>
#include <QVBoxLayout>
#include "WorldWidget.h"

/*-----------------------------------------------------------------------------
                    Main function of the program.
-----------------------------------------------------------------------------*/
int main(int argc, char *argv[]){
	// create the application
	QApplication app(argc, argv);
    glutInit(&argc, argv);

	WorldWidget myWidget;
    // myWidget.setMinimumSize(1400,1600);      //uncomment for high resolution displays
    myWidget.setMinimumSize(600,800);           //for school machines
    myWidget.show();

    // start it running
	return app.exec();
}
