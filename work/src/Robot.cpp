#include "Robot.h"
using namespace std;

/*-----------------------------------------------------------------------------
   Constructor, reads in parsed robot data from ROS which has been outputted to
   a textfile (data.txt) and uses data from this file to populate itself.
-----------------------------------------------------------------------------*/
Robot::Robot() {
    string line;
    ifstream myfile("./data.txt");
    int j=0, i=0;

    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            if(line.size()>2 && line.substr(0,2)=="::"){
                addJoint(&myfile, line, j);
                j++;
            }else  if(line.size()>2 && line.substr(0,2)=="<<"){
                addLink(&myfile, line, i);
                i++;
            }
        }
        myfile.close();
        setAxis();
    }else{
        cout << "Unable to open file";
    }
}


/*-----------------------------------------------------------------------------
    inverseKinematics using gradient descent, each robot joint angle is
    updated following the update rule Solution -= LearningRate * Gradient.
    Besides the base which can rotate 360 CW and ACW, the other 3 joints of
    the robot are capped to be within -90 and 90 giving each a total angle freedom
    of 180 degress.
------------------------------------------------------------------------------*/
void Robot::inverseKinematics (Vector3 target){
    float LearningRate=0.8, value;
    for (int i = 3; i >=0; i --)    {
        // Gradient descent Update : Solution -= LearningRate * Gradient
        float gradient = partialGradient(target, jarray, i);
        value=jarray[i]->current_angle-(LearningRate * gradient);

        if(i>0 && value<=90 && value>=-90 ){
            jarray[i]->current_angle = value;
        }else if(i==0){
            jarray[i]->current_angle = value;
        }
    }
}

/*-----------------------------------------------------------------------------
    Similar to the inverseKinematics function, this function attempts to
    correctly position the robot in order to reach the target, however, here
    only the base is updated which results in the robot alliginign itself correctly
    witht the camera. This is used just before when the robot trys to throw ball back.
------------------------------------------------------------------------------*/
void Robot::inverseKinematicsForBase (Vector3 target){
    if (distanceFromTarget(target, jarray) < 0.3){
        return;
    }

    float LearningRate=0.8;
    float gradient = partialGradient(target, jarray, 0);
    jarray[0]->current_angle =jarray[0]->current_angle-(LearningRate * gradient);
}

/*-----------------------------------------------------------------------------
    Works out the partial gradient required by the gradient descent algorithm.
------------------------------------------------------------------------------*/
float Robot::partialGradient (Robot::Vector3 target, Joint* joints[], int i){
    // Saves the angle, to restore it later on
    float angle = joints[i]->current_angle;

    float SamplingDistance=2;
    float f_x = distanceFromTarget(target, joints);

    joints[i]->current_angle += SamplingDistance;
    float f_x_plus_d = distanceFromTarget(target, joints);

    float gradient = (f_x_plus_d - f_x) / SamplingDistance;

    // Restore angle
    joints[i]->current_angle = angle;
    return gradient;
}

/*-----------------------------------------------------------------------------
    Works out the distance/length from the robots end effector which is the
    hand, to the target using the general vector length formula.
------------------------------------------------------------------------------*/
float Robot::distanceFromTarget(Vector3 target, Joint* joints[]){
    Vector3 point = forwardKinematics (joints);

    float distance=sqrt( pow(target.x-point.x,2) +   pow(target.y-point.y,2) +    pow(target.z-point.z,2));
    return distance;
}

/*-----------------------------------------------------------------------------
    forward kinematics function which given the joint angles, works out and
    returns a vector representing the position of the hand in the world. Various
    transformations are combined and are applied to the robots base position. These
    transformations are derived from the way the robot is actually rendered hierarchically.
------------------------------------------------------------------------------*/
Robot::Vector3 Robot::forwardKinematics (Joint* joints[]){
    Vector3 position;
    glm::mat4 trans = glm::mat4(1.0f);
    glm::vec4 vec3(base->position.x, base->position.y, base->position.z, 1.0f);

    //reset xyz orientation back to its default
    trans = glm::rotate(trans, glm::radians(-(360-(float)joints[0]->current_angle)), glm::vec3(.0, 1.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)joints[1]->current_angle)), glm::vec3(1.0, 0.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)joints[2]->current_angle)), glm::vec3(1.0, 0.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)joints[3]->current_angle)), glm::vec3(1.0, 0.0, 0.0));

    //go up to hand from base
    trans = glm::translate(trans, glm::vec3(.0f, 2.3, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)joints[3]->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.2, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)joints[2]->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.0, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)joints[1]->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.8, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)joints[0]->current_angle), glm::vec3(0.0, 1.0, 0.0));

    //final vector
    vec3 = trans * vec3;
    position.x = vec3.x;
    position.y = vec3.y;
    position.z = vec3.z;
    return position;
}

/*-----------------------------------------------------------------------------
            For each joint, sets the axis on which it rotates/moves.
------------------------------------------------------------------------------*/
void Robot::setAxis(){
    base->axis.x=0;
    base->axis.y=1;
    base->axis.z=0;

    joint1->axis.x=1;
    joint1->axis.y=0;
    joint1->axis.z=0;

    joint2->axis.x=1;
    joint2->axis.y=0;
    joint2->axis.z=0;

    joint3->axis.x=1;
    joint3->axis.y=0;
    joint3->axis.z=0;
}

/*-----------------------------------------------------------------------------
            Prints out all joints information on the robot.
------------------------------------------------------------------------------*/
void Robot::printAllJoints(){
    int i=0;
    for(i=0; i<5; i++){
        cout<<"\nName: "<<jarray[i]->name<<endl;
        cout<<"Parent: "<<jarray[i]->parent_link<<endl;
        cout<<"child: "<<jarray[i]->child_link<<endl;
        cout<<"current angle: "<<jarray[i]->current_angle<<endl;
        cout<<"max angle: "<<jarray[i]->max_angle<<endl;
        cout<<"joint type: "<<jarray[i]->joint_type<<endl;
        cout<<"movement type: "<<jarray[i]->movement_type<<endl;
        cout<<"mesh: "<<jarray[i]->mesh<<endl;
    }
}

/*-----------------------------------------------------------------------------
            Prints out all links information on the robot.
------------------------------------------------------------------------------*/
void Robot::printAllLinks(){
    int i=0;
    for(i=0; i<4; i++){
        cout<<"\nName: "<<larray[i]->name<<endl;
        cout<<"Parent joint name: "<<larray[i]->parent_joint<<endl;
        cout<<"child joint name: "<<larray[i]->child_joint<<endl;
        cout<<"mesh: "<<larray[i]->mesh<<endl;
    }
}

/*-----------------------------------------------------------------------------
    Reads in joint data from file and adds it to robots array of joints.
------------------------------------------------------------------------------*/
void Robot::addJoint(istream *myfile, string line, int j){
    int k;
    vector<string> data;

    for(k=0; k<6; k++){
        getline(*myfile, line);
        stringstream test(line);
        string segment;
        vector<string> seglist;

        while(getline(test, segment, ' ')){
            seglist.push_back(segment);
        }
        data.push_back(seglist[1]);
    }

    jarray[j]->name=data[0];
    jarray[j]->parent_link=data[1];
    jarray[j]->child_link=data[2];
    if(data[3]=="1"){
        jarray[j]->joint_type="REVOLUTE";   //rotation axis
    }else{
        jarray[j]->joint_type="Planar";   //normal axis
    }

    jarray[j]->movement_type=data[4];
    jarray[j]->max_angle=stoi(data[5]);
    jarray[j]->current_angle=0.0;
    jarray[j]->mesh="";
}

/*-----------------------------------------------------------------------------
    Reads in link data from file and adds it to robots array of links.
------------------------------------------------------------------------------*/
void Robot::addLink(istream *myfile, string line, int j){
    int k;
    vector<string> data;

    for(k=0; k<3; k++){
        getline(*myfile, line);
        stringstream test(line);
        string segment;
        vector<string> seglist;

        while(getline(test, segment, ' ')){
            seglist.push_back(segment);
        }
        data.push_back(seglist[1]);
    }

    larray[j]->name=data[0];
    larray[j]->parent_joint=data[1];
    larray[j]->child_joint=data[2];
    larray[j]->mesh="";
}

/*-----------------------------------------------------------------------------
                        Returns a joint given its name.
------------------------------------------------------------------------------*/
Robot::Joint* Robot::getJoint(string name){
    if(base->name==name){
        return base;
    }else if(joint1->name==name){
        return joint1;
    }else if(joint2->name==name){
        return joint2;
    }else if(joint3->name==name){
        return joint3;
    }

    return hand;
}

/*-----------------------------------------------------------------------------
                            Class Destructor.
------------------------------------------------------------------------------*/
Robot::~Robot(){
    delete base;
    delete joint1;
    delete joint2;
    delete joint3;
    delete hand;
    delete link1;
    delete link2;
    delete link3;
    delete link4;
}
