/*
 * Copyright (c) 2012, Vishal Patil
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// obtained from https://github.com/vishpat/stlviewer

#pragma once
#define STL_DBG printf
#define STL_STR_SOLID_START "solid"
#define STL_STR_SOLID_END "endsolid"
#define STL_STR_FACET_START "facet"
#define STL_STR_FACET_END "endfacet"
#define STL_STR_LOOP_START "outer"
#define STL_STR_LOOP_END "endloop"
#define STL_STR_VERTEX "vertex"

/*-----------------------------------------------------------------------------
    Parser class which originally was part of a gitbhub library for loading
    stl files in opengl. All code was in C and was converted into C++.
-----------------------------------------------------------------------------*/
class Parser {
    public:
        typedef float STLFloat;
        typedef float STLFloat32;
        typedef unsigned char STLuint8;
        typedef unsigned int STLuint32;
        typedef unsigned int STLuint;

        typedef enum {
            STL_ERR_NONE,
            STL_ERR_LOAD,
            STL_ERR_FOPEN,
            STL_ERR_FILE_FORMAT,
            STL_ERR_MEM,
            STL_ERR_NOT_LOADED,
            STL_ERR_INVALID
        } stl_error_t;

        typedef enum stl_token {
            STL_TOKEN_INVALID,
            STL_TOKEN_SOLID_START,
            STL_TOKEN_SOLID_END,
            STL_TOKEN_FACET_START,
            STL_TOKEN_FACET_END,
            STL_TOKEN_LOOP_START,
            STL_TOKEN_LOOP_END,
            STL_TOKEN_VERTEX
        } stl_token_t;

        typedef struct {
            const char *str;
            stl_token_t token;
        } stlmap;

        stlmap stl_token_map[7] = {{STL_STR_SOLID_START, STL_TOKEN_SOLID_START},
                                {STL_STR_SOLID_END, STL_TOKEN_SOLID_END},
                                {STL_STR_FACET_START, STL_TOKEN_FACET_START},
                                {STL_STR_FACET_END, STL_TOKEN_FACET_END},
                                {STL_STR_LOOP_START, STL_TOKEN_LOOP_START},
                                {STL_STR_LOOP_END, STL_TOKEN_LOOP_END},
                                {STL_STR_VERTEX, STL_TOKEN_VERTEX}};

        typedef enum stl_state {
            STL_STATE_START,
            STL_STATE_SOLID_START,
            STL_STATE_SOLID_END,
            STL_STATE_FACET_START,
            STL_STATE_FACET_END,
            STL_STATE_LOOP_START,
            STL_STATE_LOOP_END,
            STL_STATE_VERTEX
        } stl_state_t;

        typedef struct {
            STLFloat x;
            STLFloat y;
            STLFloat z;
        } vector_t;

        typedef vector_t vertex_t;
        typedef vector_t normal_t;

        typedef enum {
            STL_FILE_TYPE_INVALID,
            STL_FILE_TYPE_TXT,
            STL_FILE_TYPE_BIN
        } stl_file_type_t;

        typedef struct stl_s {
            int magic;
            char *file;
            stl_file_type_t type;
            stl_state_t state;

            STLuint32 facet_cnt;
            STLuint vertex_cnt;

            STLFloat *vertices;
            STLFloat min_x;
            STLFloat max_x;
            STLFloat min_y;
            STLFloat max_y;
            STLFloat min_z;
            STLFloat max_z;

            int lineno;
            int loaded;
        } stl_t;

        typedef struct {
            STLFloat32 x;
            STLFloat32 y;
            STLFloat32 z;
        } stl_vector_t;

 
    // functions
    stl_t *stl_alloc();
    void stl_free(stl_t *);
    stl_token_t stl_str_token(const char *str);
    stl_error_t stl_parse_txt(stl_t *stl);
    void calculate_triangle_normal(vertex_t v1, vertex_t v2, vertex_t v3,normal_t *normal);
    void stl_fill_vertex_normals(stl_t *stl);
    stl_error_t stl_get_vertices(stl_t *stl);
    stl_file_type_t stl_get_filetype(char *filename);
    stl_error_t stl_load_bin_file(stl_t *stl);
    stl_error_t stl_load_txt_file(stl_t *stl);
    stl_error_t stl_load(stl_t *stl, char *filename);

    STLFloat stl_min_x(stl_t *stl) { return stl->min_x; };
    STLFloat stl_max_x(stl_t *stl) { return stl->max_x; };
    STLFloat stl_min_y(stl_t *stl) { return stl->min_y; };
    STLFloat stl_max_y(stl_t *stl) { return stl->max_y; };
    STLFloat stl_min_z(stl_t *stl) { return stl->min_z; };
    STLFloat stl_max_z(stl_t *stl) { return stl->max_z; };
    STLuint stl_facet_cnt(stl_t *stl) { return stl->facet_cnt; };
    STLuint stl_vertex_cnt(stl_t *stl) { return stl->vertex_cnt; };

    stl_error_t stl_vertices(stl_t *stl, STLFloat **points) {
        if (stl->loaded == 0) {
            return STL_ERR_NOT_LOADED;
        }

        *points = stl->vertices;

        return STL_ERR_NONE;
    };

    int stl_error_lineno(stl_t *stl) { return stl->lineno; };
};
