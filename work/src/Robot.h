#pragma once
#include <stdio.h>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

/*-----------------------------------------------------------------------------
    Class for representing the robot and all of its necessary information such
    as joints, angles and to perform inverse Kinematics.
-----------------------------------------------------------------------------*/
class Robot {
    public:
        //structs
        typedef struct {
            float x, y, z;
        } Vector3;

        typedef struct {
            string name;
            string parent_joint;
            string child_joint;
            string mesh;
        } Link;

        typedef struct Joint {
            string parent_link;
            string child_link;
            string name;
            float current_angle;
            int max_angle;
            string joint_type;
            string movement_type;
            string mesh;

            Vector3 position;
            Vector3 axis;
            Vector3 startoffset;
        } Joint;

        //functions
        Robot();
        ~Robot();
        void addJoint(istream *myfile, string line, int j);
        void addLink(istream *myfile, string line, int j);
        Robot::Joint *getJoint(string name);
        void printAllJoints();
        void printAllLinks();
        void setAxis();

        Robot::Vector3 forwardKinematics(Joint *joints[]);
        float distanceFromTarget(Vector3 target, Joint *joints[]);
        float partialGradient(Vector3 target, Joint *joints[], int i);
        void inverseKinematics(Vector3 target);
        void inverseKinematicsForBase(Vector3 target);

        string name;
        int total_links;
        int total_joints;

        Joint *base = new Joint();
        Joint *joint1 = new Joint();
        Joint *joint2 = new Joint();
        Joint *joint3 = new Joint();
        Joint *hand = new Joint();
        Joint *jarray[5] = {base, joint1, joint2, joint3, hand};

        Link *link1 = new Link();
        Link *link2 = new Link();
        Link *link3 = new Link();
        Link *link4 = new Link();
        Link *larray[4] = {link1, link2, link3, link4};
};
