#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1
#include <GL/glut.h>
#include <math.h>
#include <QGLWidget>
#include <QtGui>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QElapsedTimer>

#include "Image.h"
#include "Robot.h"
#include "Parser.h"
#include "Ball.h"

/*-----------------------------------------------------------------------------
    Header file and class declaration for WorldWidget.cpp. This class extends
    QGLWidget in order to make use of QT's opengl features and, displays all
    necessary robot graphics such as the meshes and textures etc. 
-----------------------------------------------------------------------------*/
class WorldWidget : public QGLWidget {
    Q_OBJECT
   public: 
        WorldWidget(QWidget* parent = 0);
        ~WorldWidget();
        void resizeGL(int w, int h);
        void renderRobot();
        void mousePressEvent(QMouseEvent* eventPress);
        void mouseReleaseEvent(QMouseEvent *eventPress);

        void updateJoint1Position();
        void updateJoint2Position();
        void updateJoint3Position();
        void updateHandPosition();
        void calculateEstimate();

        void drawTriangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2,GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3,GLfloat z3);
        void initMesh(char *filename, int index);
        void renderMeshes();
        void drawMesh(int index);
        void renderBalls();
        void handAnimation();
        void throwBall();

        void initializeGL();
        void paintGL();
        void setMaterial(int style);  
        void displayFloor();
        void displayxyz();
        void keyPressEvent(QKeyEvent* ev);
        void showInstructions();

    public slots:
        void sliderUpdate(int value);
        void robotGoTo();
        void allignRobot();

    private:
        int update_rate=40;
        Image floortop;
        GLuint tex_obj[1];
        int rotate_angle = 0;
        QTimer *timer_go, *timer_throwback;
        float temp = 0.0002;

        //camera info
        float camera[3], lookat[3]={0.0, 0.0, 0.0}, eye[3]={0.0,1.0,0.0};  

        //objects used for skeleton version of robot
        GLUquadricObj* base = gluNewQuadric();
        GLUquadricObj* link1 = gluNewQuadric();
        GLUquadricObj* joint1 = gluNewQuadric();
        GLUquadricObj* link2 = gluNewQuadric();
        GLUquadricObj* joint2 = gluNewQuadric();
        GLUquadricObj* link3 = gluNewQuadric();
        GLUquadricObj* joint3 = gluNewQuadric();
        GLUquadricObj* link4 = gluNewQuadric();
        GLUquadricObj* hand = gluNewQuadric();
        float jointgap=0.5;
        float linksize=1.5;

        Robot* robot = new Robot();
        Robot::Vector3 target;
        float* joint = &robot->base->current_angle;
        float max_base_distance=5.0, min_goal=0.3, max_hand_distance=10.3; //can tolerate max base dist=5.5,      max hand dist=10.8  if straight up

        Parser *parser=new Parser();
        int i, base2;
        Parser::stl_t* base_mesh=NULL;
        Parser::stl_t* link1_mesh=NULL;
        Parser::stl_t* joint1_mesh=NULL;
        Parser::stl_t* link2_mesh=NULL;
        Parser::stl_t* j2_l3_mesh=NULL;
        Parser::stl_t* J3_l4_mesh=NULL;
        Parser::stl_t* hand_mesh=NULL;
        Parser::stl_t* mesh_array[7]={base_mesh, link1_mesh, joint1_mesh, link2_mesh, j2_l3_mesh, J3_l4_mesh, hand_mesh};

        Ball *ball=NULL;
        bool ballmade=false, ballfinished=false, got_estimate=false, straighten=false, inposition=false, forward=false;
        GLdouble clickedx, clickedy, clickedz;
        QElapsedTimer timer;
        float base_speed=0.00001, xdiff, zdiff;
        Robot::Vector3 estimate;
};

/*-----------------------------------------------------------------------------
                    Setting up different material properties.
-----------------------------------------------------------------------------*/
typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} materialStruct;

static materialStruct brassMaterials = {
    {0.33, 0.22, 0.03, 1.0}, {0.78, 0.57, 0.11, 1.0}, {0.99, 0.91, 0.81, 1.0}, 27.8};

static materialStruct whiteShinyMaterials = {
    {1.0, 1.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}, 100.0};

static materialStruct red_shiny_plastic{
    {0.3, 0.0, 0.0, 1.0},{0.6, 0.0, 0.0, 1.0},{0.8, 0.6, 0.6, 1.0},100.0};

static materialStruct white_rubber{
    { 0.05f,0.05f,0.05f,1.0f },{ 0.5f,0.5f,0.5f,1.0f},{ 0.7f,0.7f,0.7f,1.0f},10.0f};
#endif
