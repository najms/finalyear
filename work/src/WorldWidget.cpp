#include <GL/glu.h>
#include "WorldWidget.h"
#include <iostream>
#include <QTimer>
#include <stdio.h>
using namespace std;

/*-----------------------------------------------------------------------------
Constructor, initaliases all necessary properties and loads meshes into mesh array.
-----------------------------------------------------------------------------*/
WorldWidget::WorldWidget(QWidget *parent): QGLWidget(parent), floortop("./textures/grassfront.png", false){
    QTimer *timer_rotation=new QTimer(this);
    connect(timer_rotation, SIGNAL(timeout()), this, SLOT(update()));
    timer_rotation->start(update_rate);

    camera[0]=15;
    camera[1]=10;
    camera[2]=0;
    this->setMouseTracking(true);
    this->setFocusPolicy(Qt::StrongFocus);

    robot->base->current_angle=0;
    robot->joint1->current_angle=0;
    robot->joint2->current_angle=0;
    robot->joint3->current_angle=0;

    robot->base->position.x = 0;
    robot->base->position.y = 0;
    robot->base->position.z = 0;

    timer_go=new QTimer(this);
    connect(timer_go, SIGNAL(timeout()), this, SLOT(robotGoTo()));
    timer_go->setInterval(0);

    timer_throwback=new QTimer(this);
    connect(timer_throwback, SIGNAL(timeout()), this, SLOT(allignRobot()));
    timer_throwback->setInterval(2);

    showInstructions();
}

void WorldWidget::showInstructions(){
    printf("\nWelcome, click somewhere on the floor to begin!\n");
    printf("How long you click determines the how far the ball will go\n");
    printf("press r to reset ball and have another go\n\n");
    printf("Use the left and right arrow keys to manually modify joint angles\n");
    printf("Use 1,2,3 or 4 to set which joint is being manipulated\n");
    printf("Use w,a,s,d to move camera around, q to bring camera down, e to lift camera up\n\n");

    initMesh("./textures/base_mesh.stl", 0);
    initMesh("./textures/link1_mesh.stl", 1);
    initMesh("./textures/joint1_mesh.stl", 2);
    initMesh("./textures/link2_mesh.stl", 3);
    initMesh("./textures/j2_l3_mesh.stl", 4);
    initMesh("./textures/j3_l4_mesh.stl", 5);
    initMesh("./textures/solid_hand_mesh.stl", 6);
}

/*-----------------------------------------------------------------------------
    PaintGL function which is called every 40ms by timer, calls relevant
    functions to render robot and its movement, and all other required functions.
-----------------------------------------------------------------------------*/
void WorldWidget::paintGL(){    //opengl is X Y Z
    // clear the widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);

    //Lighting
	glPushMatrix();
	glLoadIdentity();
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glPopMatrix();

    this->displayFloor();

    glPushMatrix();
    glTranslatef(robot->base->position.x, robot->base->position.y,  robot->base->position.z);
    renderMeshes();
    this->updateJoint1Position();
    this->updateJoint2Position();
    this->updateJoint3Position();
    this->updateHandPosition();
    glPopMatrix();

    this->renderBalls();

    if(straighten==true){
        allignRobot();
    }else if(inposition==true){
        handAnimation();
    }

    glLoadIdentity();
    gluLookAt(camera[0],camera[1],camera[2],    lookat[0],lookat[1],lookat[2],        eye[0], eye[1], eye[2]);
    glRotatef(this->rotate_angle,0.,1.0,0.);
	glFlush();
}

/*-----------------------------------------------------------------------------
    Modifies joint3 angle so that it goes backward at a constant rate and then
    comes back forward at a faster rate in an attempt to show the ball being
    thrown back towards the camera.
-----------------------------------------------------------------------------*/
void WorldWidget::handAnimation(){
    if(robot->base->current_angle>0){
        if(robot->joint3->current_angle>-90 && forward==false){
            robot->joint3->current_angle-=3;
        }else{
            forward=true;
        }

        if(forward==true && robot->joint3->current_angle<40 ){
            robot->joint3->current_angle+=8;
        }else if(forward==true){
            inposition=false;
            throwBall();
        }
    }else{
        if(robot->joint3->current_angle<90 && forward==false){
            robot->joint3->current_angle+=3;
        }else{
            forward=true;
        }

        if(forward==true && robot->joint3->current_angle>-40 ){
            robot->joint3->current_angle-=8;
        }else if(forward==true){
            inposition=false;
            throwBall();
        }
    }
}

/*-----------------------------------------------------------------------------
    Reassigns the position of the ball and its velocity. Velocity is worked
    out by getting a unit vector in the direction towards the camera. The y
    value of the velocity is added an extra float value when necessary so that
    the ball reaches the camera regardless of the robots position.
-----------------------------------------------------------------------------*/
void WorldWidget::throwBall(){
    //reassign ball values
    ball->position.x = robot->hand->position.x;
    ball->position.y = robot->hand->position.y+0.7;
    ball->position.z = robot->hand->position.z;

    Robot::Vector3 direction ={(camera[0]-5)-ball->position.x, 0-ball->position.y, camera[2]-ball->position.z };
    float length = sqrt((direction.x * direction.x) + (direction.y * direction.y) + (direction.z * direction.z));  // a scalar value
    Robot::Vector3 unit = {direction.x/length, direction.y/length,  direction.z/length};

    ball->velocity.x=unit.x;
    ball->velocity.y=unit.y;
    ball->velocity.z=unit.z;
    printf("\nThrowing ball back!\n");

    //estimate and add small value to velocity.y if necessary
    estimate={ball->position.x, ball->position.y, ball->position.z};
    Robot::Vector3 velocity={ball->velocity.x, ball->velocity.y, ball->velocity.z};
    Robot::Vector3 direction2;
    float length2, toadd=0.0;

    while(true){ //10 time steps each cycle
        velocity.y=velocity.y-0.1;
        estimate.x+=velocity.x*4;
        estimate.y+=velocity.y*4;
        estimate.z+=velocity.z*4;

        if(estimate.y<=0.5){
            direction2.x=(camera[0]-5)-estimate.x;
            direction2.z=camera[2]-estimate.z;
            length2 = sqrt((direction2.x * direction2.x) + (direction2.z * direction2.z));

            if(length2<=4){
                ball->velocity.y+=toadd;
                break;
            }else{
                toadd+=0.1;
                estimate={ball->position.x, ball->position.y, ball->position.z};
                velocity={ball->velocity.x, ball->velocity.y+toadd, ball->velocity.z};
            }
        }
    }

    //start ball throwing
    ball->active=true;
    ballfinished=false;
}

/*-----------------------------------------------------------------------------
    After the ball has been catched, this is called to allign the robots front
    with the camera and then brings all joint angles to 0.
-----------------------------------------------------------------------------*/
void WorldWidget::allignRobot(){
    //allign robot with camera
    if(straighten==false){
        target.x=camera[0];
        target.y=camera[1];
        target.z=camera[2];

        temp=robot->base->current_angle;
        robot->inverseKinematicsForBase(target);
        if(abs(robot->base->current_angle-temp)<=0.005){
            timer_throwback->stop();
            straighten=true;
        }
    }

    //bring all joint angles to 0
    if(straighten==true){
        if(robot->joint1->current_angle!=0 ){
            robot->joint1->current_angle+=robot->joint1->current_angle<0 ? 1 : -1;
        }
        if(robot->joint2->current_angle!=0){
            robot->joint2->current_angle+=robot->joint2->current_angle<0 ? 1 : -1;
        }
        if(robot->joint3->current_angle!=0){
            robot->joint3->current_angle+=robot->joint3->current_angle<0 ? 1 : -1;
        }

        if(abs(robot->joint1->current_angle+robot->joint2->current_angle+robot->joint3->current_angle) <3){
            straighten=false;
            printf("\nRobot straightened\n");
            inposition=true;
        }
    }
}

/*-----------------------------------------------------------------------------
    Renders the ball in the world by callings its draw function and, updates
    the position of the ball using the balls move function.
-----------------------------------------------------------------------------*/
void WorldWidget::renderBalls(){
    if(!ballmade || ballfinished==true)
        return;

    setMaterial(3);
    ball->draw(true);
    ball->move();

    if(ball->active==false)
        return;

    //ball reaches floor
    if(ball->position.y<=0.5){
            ball->active=false;
    }

    if(ball->position.y<=3){
        base_speed=0.00003;
    }

    //set targets new pos
    target.x=ball->position.x;
    target.y=ball->position.y;
    target.z=ball->position.z;
}

/*-----------------------------------------------------------------------------
    When mouse is released, ball object gets created and its position and
    velocity are set. The velocity is a unit vector in the direction towards
    where the mouse was clicked. Velocity.y is added an extra value depending
    on how long the mouse was held down for. This ensures that long clicks result
    in ball being thrown further whereas a fast click results in smaller range.
-----------------------------------------------------------------------------*/
void WorldWidget::mouseReleaseEvent(QMouseEvent *eventPress){
    if(ballmade==false){
        //convert mouse click to world coordinates
        ballmade=true;
        GLint viewport[4];
        GLdouble modelview[16];
        GLdouble projection[16];
        GLfloat winX, winY, winZ;

        glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
        glGetDoublev( GL_PROJECTION_MATRIX, projection );
        glGetIntegerv( GL_VIEWPORT, viewport );

        winX = (float)eventPress->x();
        winY = (float)viewport[3] - (float)eventPress->y();
        glReadPixels( eventPress->x(), int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

        gluUnProject( winX, winY, winZ, modelview, projection, viewport, &clickedx, &clickedy, &clickedz);

        //get unit vector  and create ball object
        Robot::Vector3 direction ={clickedx-camera[0], clickedy-camera[1], clickedz-camera[2] };
        float length = sqrt((direction.x * direction.x) + (direction.y * direction.y) + (direction.z * direction.z));  // a scalar value
        Robot::Vector3 unit = {direction.x/length, direction.y/length,  direction.z/length};

        ball=new Ball(camera[0]-2,   camera[1]-2,   camera[2]);
        ball->velocity.x=unit.x;
        ball->velocity.y=unit.y+(timer.elapsed()/1000.0>1.5 ? 1.5 : timer.elapsed()/1000.0);
        ball->velocity.z=unit.z;

        timer.invalidate();
        printf("\nPerforming Ball throwing....\n");
        timer_go->start();
    }
}

/*-----------------------------------------------------------------------------
    A simple timer is started to calculate how long the mouse was clicked.
-----------------------------------------------------------------------------*/
void WorldWidget::mousePressEvent(QMouseEvent *eventPress){
    timer.start();
}

/*-----------------------------------------------------------------------------
    Called by timer to move the robots base position to where the ball will
    land and also, calls inverse kinematic function to make robot catch ball.
-----------------------------------------------------------------------------*/
void WorldWidget::robotGoTo(){
    //ball is catched
    if(robot->distanceFromTarget(ball->position, robot->jarray)<=min_goal){
        cout<<"\nBall has been catched, Robot wins! :)"<<endl;
        timer_go->stop();
        ball->active=false;
        ballfinished=true;
        timer_throwback->start();
        return;
    }

    //estimate where ball will land
    if(got_estimate==false){
        got_estimate=true;
        calculateEstimate();
    }

    //move robots base position towards where the ball will land
    xdiff=estimate.x-robot->base->position.x;
    zdiff=estimate.z-robot->base->position.z;
    robot->base->position.x+=xdiff*base_speed;
    robot->base->position.z+=zdiff*base_speed;

    //IK
    robot->inverseKinematics(target);
}

/*-----------------------------------------------------------------------------
    This is called to workout where the ball will land given its inital position
    and velocity. A while loop is used where each loop is equivelant to 20 time steps
    where each time step is the same as calling the move function on the ball once.
-----------------------------------------------------------------------------*/
void WorldWidget::calculateEstimate(){//float gravity=0.01, speed=0.4;
   estimate={ball->position.x, ball->position.y, ball->position.z};
   Robot::Vector3 velocity={ball->velocity.x, ball->velocity.y, ball->velocity.z};

   while(true){ //20 time steps each cycle
        velocity.y=velocity.y-0.2;
        estimate.x+=velocity.x*8;
        estimate.y+=velocity.y*8;
        estimate.z+=velocity.z*8;
        if(estimate.y<=0.5){
            break;
        }
   }
}

/*-----------------------------------------------------------------------------
    Displays the complete robot on screen with its correction position and joint
    angles. It does this by rendering each of the robots meshes one after the
    other in a hierarchical fashion.
-----------------------------------------------------------------------------*/
void WorldWidget::renderMeshes(){
    glDisable(GL_TEXTURE_2D);
    glPushMatrix();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);

    glRotatef(robot->base->current_angle,0.,1.,0.);         //base rotation
    // displayxyz();
        //base
        setMaterial(0);
        glPushMatrix();
        glScalef(10.0, 10.0, 10.0);
        glRotatef(270.,1.,0.,0.);
        drawMesh(0);
        glPopMatrix();

        //link1
        setMaterial(1);
        glTranslatef(0.0, 0.8,  0.0);
        glPushMatrix();
        glScalef(10.0, 10.0, 10.0);
        glRotatef(270.,1.,0.,0.);
        drawMesh(1);
        glPopMatrix();

        //joint1
        setMaterial(0);
        glPushMatrix();
        glScalef(10.0, 10.0, 10.0);
        glRotatef(-90.,1.,0.,0.);
        glRotatef(90.,0.,0.,1.);
        drawMesh(2);
        glPopMatrix();

        glTranslatef(0.0, 2.0,  0.0);
        glRotatef(robot->joint1->current_angle,1.,0.,0.);       //j1 angle
        // displayxyz();

            //link2
            setMaterial(1);
            glPushMatrix();
            glScalef(10.0, 10.0, 10.0);
            glRotatef(-90,1.,0.,0.);
            glRotatef(90.,0.,0.,1.);
            glRotatef(-90.,0.,1.,0.);
            drawMesh(3);
            glPopMatrix();

            glTranslatef(0.0, 2.0,  0.0);
            glRotatef(robot->joint2->current_angle,1.,0.,0.);   //j2 angle
            // displayxyz();

                //joint2 and link3
                setMaterial(0);
                glPushMatrix();
                glScalef(10.0, 15.0, 10.0);
                glRotatef(-90,1.,0.,0.);
                glRotatef(90.,0.,0.,1.);
                glRotatef(-90.,0.,1.,0.);
                drawMesh(4);
                glPopMatrix();

                glTranslatef(0.0, 2.2,  0.0);
                glRotatef(robot->joint3->current_angle,1.,0.,0.);   //j3 angle
                // displayxyz();

                    //joint3 and link4
                    glPushMatrix();
                    glScalef(10.0, 15.0, 10.0);
                    glRotatef(-90,1.,0.,0.);
                    glRotatef(90.,0.,0.,1.);
                    glRotatef(-90.,0.,1.,0.);
                    drawMesh(5);
                    glPopMatrix();

                        //hand
                        setMaterial(2);
                        glTranslatef(0.0, 2.0,  0.0);
                        glPushMatrix();
                        glRotatef(-90,1.,0.,0.);
                        glScalef(10.0, 10.0, 10.0);
                        drawMesh(6);
                        glPopMatrix();
                        // displayxyz();

                        if(ballfinished){
                            glTranslatef(0.0, 0.7,  0.0);
                            setMaterial(3);
                            ball->draw(false);
                        }
    glPopMatrix();
    glEnable(GL_TEXTURE_2D);
}

/*-----------------------------------------------------------------------------
            Forward Kinematics to update robots hand position.
-----------------------------------------------------------------------------*/
void WorldWidget::updateHandPosition(){
    //End Effector--hand
    glm::mat4 trans = glm::mat4(1.0f);
    glm::vec4 vec3(robot->base->position.x, robot->base->position.y, robot->base->position.z, 1.0f);

    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->base->current_angle)), glm::vec3(.0, 1.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint1->current_angle)), glm::vec3(1.0, 0.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint2->current_angle)), glm::vec3(1.0, 0.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint3->current_angle)), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.3, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint3->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.2, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint2->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.0, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint1->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.8, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->base->current_angle), glm::vec3(.0, 1.0, 0.0));

    vec3 = trans * vec3;
    robot->hand->position.x = vec3.x;
    robot->hand->position.y = vec3.y;
    robot->hand->position.z = vec3.z;
}

/*-----------------------------------------------------------------------------
            Forward Kinematics to update robots joint1 position.
-----------------------------------------------------------------------------*/
void WorldWidget::updateJoint1Position(){
    //joint1
    glm::vec4 vec(robot->base->position.x, robot->base->position.y, robot->base->position.z, 1.0f);
    glm::mat4 trans = glm::mat4(1.0f);
    trans = glm::rotate(trans, glm::radians(-(float)robot->base->current_angle), glm::vec3(.0, 1.0, 0.0));
    trans = glm::translate(trans, glm::vec3(.0f, 0.8, 0.0f));
    trans = glm::rotate(trans, glm::radians((float)robot->base->current_angle), glm::vec3(.0, 1.0, 0.0));

    vec = trans * vec;
    robot->joint1->position.x = vec.x;
    robot->joint1->position.y = vec.y;
    robot->joint1->position.z = vec.z;
}

/*-----------------------------------------------------------------------------
            Forward Kinematics to update robots joint2 position.
-----------------------------------------------------------------------------*/
void WorldWidget::updateJoint2Position(){
    //Joint2
    glm::mat4 trans = glm::mat4(1.0f);
    glm::vec4 vec4(robot->base->position.x, robot->base->position.y, robot->base->position.z, 1.0f);

    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->base->current_angle)), glm::vec3(.0, 1.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint1->current_angle)), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 4.0, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint1->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 0.8, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->base->current_angle), glm::vec3(.0, 1.0, 0.0));

    vec4 = (trans * vec4);
    robot->joint2->position.x = vec4.x;
    robot->joint2->position.y = vec4.y;
    robot->joint2->position.z = vec4.z;
}

/*-----------------------------------------------------------------------------
            Forward Kinematics to update robots joint3 position.
-----------------------------------------------------------------------------*/
void WorldWidget::updateJoint3Position(){
    //joint3
    glm::mat4 trans = glm::mat4(1.0f);
    glm::vec4 vec2(robot->base->position.x, robot->base->position.y, robot->base->position.z, 1.0f);

    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->base->current_angle)), glm::vec3(.0, 1.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint1->current_angle)), glm::vec3(1.0, 0.0, 0.0));
    trans = glm::rotate(trans, glm::radians(-(360-(float)robot->joint2->current_angle)), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.2, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint2->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.0, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->joint1->current_angle), glm::vec3(1.0, 0.0, 0.0));

    trans = glm::translate(trans, glm::vec3(.0f, 2.8, 0.0f));
    trans = glm::rotate(trans, glm::radians(-(float)robot->base->current_angle), glm::vec3(.0, 1.0, 0.0));

    vec2 = trans * vec2;
    robot->joint3->position.x = vec2.x;
    robot->joint3->position.y = vec2.y;
    robot->joint3->position.z = vec2.z;
}

/*-----------------------------------------------------------------------------
    Similar to renderMeshes function, this function displays the robot in a
    skeleton form using spheres and cyclinders, also displays coordinate frames.
-----------------------------------------------------------------------------*/
void WorldWidget::renderRobot(){
    glDisable(GL_TEXTURE_2D);
	glPushMatrix();

    glRotatef(robot->base->current_angle,0.,1.,0.);  //base rotation
    displayxyz();

        //base
        setMaterial(2);
        glRotatef(90.,1.,0.,0.);
        gluCylinder(base,  0.7,  0.7,  0.5,  20,  20);
        glRotatef(-90.,1.,0.,0.);

        //link1
        setMaterial(1);
        glTranslatef(0.0, 1.5,  0.0);
        glRotatef(90.,1.,0.,0.);
        gluCylinder(link1,  0.3,  0.3,  linksize,  4,  4);
        glRotatef(-90.,1.,0.,0.);

        //joint1
        setMaterial(0);
        glTranslatef(0.0, jointgap,  0.0);
        gluSphere(joint1,0.4,20,20);

        glRotatef(robot->joint1->current_angle,1.,0.,0.);   //j1 angle
        displayxyz();

            //link2
            setMaterial(1);
            glTranslatef(0.0, jointgap,  0.);
            glRotatef(270,1.,0.,0.);
            gluCylinder(link2,  0.3,  0.3,  1.5,  4,  4);
            glRotatef(-270,1.,0.,0.);

            //joint2
            setMaterial(0);
            glTranslatef(0.0, jointgap+linksize,  0.);
            gluSphere(joint2,0.4,20,20);

            glRotatef(robot->joint2->current_angle,1.,0.,0.);   //j2 angle
            displayxyz();

                //link3
                setMaterial(1);
                glTranslatef(0.0, jointgap,  0.);
                glRotatef(270,1.,0.,0.);
                gluCylinder(link3,  0.3,  0.3,  1.5,  4,  4);
                glRotatef(-270,1.,0.,0.);

                //joint3
                setMaterial(0);
                glTranslatef(0.0, jointgap+linksize,  0.);
                gluSphere(joint3,0.4,20,20);

                glRotatef(robot->joint3->current_angle,1.,0.,0.);   //j3 angle
                displayxyz();

                    //link4
                    setMaterial(1);
                    glTranslatef(0.0, jointgap,  0.);
                    glRotatef(270.,1.,0.,0.);
                    gluCylinder(link4,  0.3,  0.3,  1.5,  4,  4);
                    glRotatef(-270.,1.,0.,0.);

                    //hand
                    setMaterial(2);
                    glTranslatef(0.0,  jointgap+linksize, 0.);
                    gluSphere(hand,0.4,10,10);

	glPopMatrix();
    glEnable(GL_TEXTURE_2D);
}

/*-----------------------------------------------------------------------------
    Given an index for the mesh array, draws all the triangles in the mesh
    using GL_TRIANGLES.
-----------------------------------------------------------------------------*/
void WorldWidget::drawMesh(int index){
    glPushMatrix();
    float* vertices=mesh_array[index]->vertices;
    glBegin(GL_TRIANGLES);
    for (i = 0; i < (int) mesh_array[index]->facet_cnt; i++) {
        base2 = i * 18;
        drawTriangle(vertices[base2], vertices[base2 + 1], vertices[base2 + 2],
                     vertices[base2 + 6], vertices[base2 + 7], vertices[base2 + 8],
                     vertices[base2 + 12], vertices[base2 + 13],vertices[base2 + 14]);
    }
    glEnd();
    glPopMatrix();
}

/*-----------------------------------------------------------------------------
   Called by drawMesh function and takes as argument the xyz coordinates for
   each of the triangles vertices. This function works out the correct normal
   values and calls glVertex3f to complete the traingle drawing.
-----------------------------------------------------------------------------*/
void WorldWidget::drawTriangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2,
                         GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3,GLfloat z3) {
    Robot::Vector3 normal;
    Robot::Vector3 U;
    Robot::Vector3 V;
    GLfloat length;

    U.x = x2 - x1;
    U.y = y2 - y1;
    U.z = z2 - z1;

    V.x = x3 - x1;
    V.y = y3 - y1;
    V.z = z3 - z1;

    normal.x = U.y * V.z - U.z * V.y;
    normal.y = U.z * V.x - U.x * V.z;
    normal.z = U.x * V.y - U.y * V.x;

    length = normal.x * normal.x + normal.y * normal.y + normal.z * normal.z;
    length = sqrt(length);

    glNormal3f(normal.x / length, normal.y / length, normal.z / length);
    glVertex3f(x1, y1, z1);
    glVertex3f(x2, y2, z2);
    glVertex3f(x3, y3, z3);
}

/*-----------------------------------------------------------------------------
    Given the filename of a mesh and a index into the mesh array, uses the
    Parser class to load the mesh and all of its data.
-----------------------------------------------------------------------------*/
void WorldWidget::initMesh(char *filename, int index) {
    Parser::stl_error_t err;
    mesh_array[index] = parser->stl_alloc();
    if (mesh_array[index] == NULL) {
        fprintf(stderr, "Unable to allocate memoryfor the stl object");
        exit(1);
    }
    err = parser->stl_load(mesh_array[index], filename);
}

/*-----------------------------------------------------------------------------
                                Class Destructor.
-----------------------------------------------------------------------------*/
WorldWidget::~WorldWidget(){
    cout<<"\nReleasing Memory....\n";
    gluDeleteQuadric(base);
    gluDeleteQuadric(link1);
    gluDeleteQuadric(joint1);
    gluDeleteQuadric(link2);
    gluDeleteQuadric(joint2);
    gluDeleteQuadric(link3);
    gluDeleteQuadric(joint3);
    gluDeleteQuadric(link4);
    gluDeleteQuadric(hand);

    delete robot;
    delete ball;
    delete base_mesh;
    delete link1_mesh;
    delete joint1_mesh;
    delete link2_mesh;
    delete j2_l3_mesh;
    delete J3_l4_mesh;
    delete hand_mesh;
}

/*-----------------------------------------------------------------------------
                    Sets the worlds background color.
-----------------------------------------------------------------------------*/
void WorldWidget::initializeGL(){
    // glClearColor(0.3, 0.3, 0.3, 0.0);    //grey
    glClearColor(1, 1, 1, 0.0);     //white
}

/*-----------------------------------------------------------------------------
    Key press event handler, different keys can be used to control the camera
    aswell as manually modify the joint angles.
-----------------------------------------------------------------------------*/
void WorldWidget::keyPressEvent(QKeyEvent *ev){
    //left and right arrows for changing angles
    if (ev->key() == Qt::Key_Left) {
        if(*joint>-90)
        *joint=*joint-1;
    }
    if (ev->key() == Qt::Key_Right) {
            if(*joint<90)
        *joint=*joint+1;
    }

    //w,a,s,d,q,e for move camera forward, left, backward, right, up and down
    if(ev->key() == Qt::Key_W){         //move camera forward
        camera[0]-=1;
        lookat[0]-=1;
    }else if(ev->key() == Qt::Key_S){   //move camera backward
        camera[0]+=1;
        lookat[0]+=1;
    }else if(ev->key() == Qt::Key_A){   //move camera left
        camera[2]+=1;
        lookat[2]+=1;
    }else if(ev->key() == Qt::Key_D){   //move camera right
        camera[2]-=1;
        lookat[2]-=1;
    }else if(ev->key() == Qt::Key_E){   //move camera up
        camera[1]+=0.5;
        lookat[1]+=0.5;
    }else if(ev->key() == Qt::Key_Q){   //move camera down
        if(camera[1]>2){
            camera[1]-=0.5;
            lookat[1]-=0.5;
        }
    }

    //r to reset environment
    if (ev->key() == Qt::Key_R) {
        camera[0]=15;
        camera[1]=10;
        camera[2]=0;
        lookat[0]=0.0;
        lookat[1]=0.0;
        lookat[2]=0.0;

        ballmade=false;
        if(ball!=NULL){
            ball->active=false;
            delete ball;
            ball=NULL;
        }
        base_speed=0.00001;
        ballfinished=false, got_estimate=false, straighten=false, inposition=false, forward=false;

        this->rotate_angle=0;
        timer_go->stop();
        timer_throwback->stop();
        printf("\nResetting....\n");
    }

    //up and down arrows for raising and bring camera down
    if (ev->key() == Qt::Key_Up) {
        if(camera[1]>4){
            camera[0]-=1;
            camera[1]-=1;
        }
    }else if (ev->key() == Qt::Key_Down) {
        camera[0]+=1;
        camera[1]+=1;
    }

    //change which joint angle is being modified
    if (ev->key() == Qt::Key_1){
        joint=&robot->base->current_angle;
    }else if (ev->key() == Qt::Key_2){
        joint=&robot->joint1->current_angle;
    }else if (ev->key() == Qt::Key_3){
        joint=&robot->joint2->current_angle;
    }else if (ev->key() == Qt::Key_4){
        joint=&robot->joint3->current_angle;
    }
}

/*-----------------------------------------------------------------------------
        Slot for slider which can be used to rotate around the y axis
-----------------------------------------------------------------------------*/
void WorldWidget::sliderUpdate(int value){
    this->rotate_angle=value;
}

/*-----------------------------------------------------------------------------
Sets the materials properties for a particular material determined by the parameter.
-----------------------------------------------------------------------------*/
void WorldWidget::setMaterial(int style){   //brass=0   white=1     red=2   rubber=3
    materialStruct* p_front;
    p_front= &brassMaterials;

    if(style==0){
        p_front= &brassMaterials;
    }else if(style==1){
        p_front= &whiteShinyMaterials;
    }else if(style==2){
        p_front= &red_shiny_plastic;
    }else if(style==3){
        p_front= &white_rubber;
    }

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
}

/*-----------------------------------------------------------------------------
Called every time the widget is resized, sets lighting, textures and frustum view.
-----------------------------------------------------------------------------*/
void WorldWidget::resizeGL(int w, int h){
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    //enable features
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    //set up lighting
    GLfloat light_pos[] = {0., 0., 10., 1.};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);

    //load and save all textures
    glGenTextures(1,this->tex_obj);
    glBindTexture(GL_TEXTURE_2D, tex_obj[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floortop.Width(), floortop.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floortop.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    // left, right, bottom, top, zNear, zFar
    glFrustum(-1.0,1.0, -1.0, 1.0, 1.5, 200.0);
    glMatrixMode(GL_MODELVIEW);
}

/*-----------------------------------------------------------------------------
    Creates the floor in the world which is basically a wide square texture.
-----------------------------------------------------------------------------*/
void WorldWidget::displayFloor(){
    glEnable(GL_TEXTURE_2D);
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };
    this->setMaterial(1);

    //bind floor texture then load it into the graphics hardware
    glBindTexture(GL_TEXTURE_2D,tex_obj[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floortop.Width(), floortop.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floortop.imageField());

    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f(  100.0,  0.0,  100.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f(  100.0,  0.0, -100.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( -100.0,  0.0, -100.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( -100.0,  0.0,  100.0);
    glEnd();
}

/*-----------------------------------------------------------------------------
                Using simple lines, displays the xyz axis.
-----------------------------------------------------------------------------*/
void WorldWidget::displayxyz(){
    int length=2;
        glColor3f(1.0,0.0,0.0);
        glBegin(GL_LINES);
        // x aix
        glVertex3f(0, 0.0f, 0.0f);
        glVertex3f(length, 0.0f, 0.0f);

        // arrow
        glVertex3f(length, 0.0f, 0.0f);
        glVertex3f(length-0.5, 0.5f, 0.0f);
        glVertex3f(length, 0.0f, 0.0f);
        glVertex3f(length-0.5, -0.5f, 0.0f);
        glEnd();
        glFlush();

        // y
        glColor3f(0.0,1.0,0.0);
        glBegin(GL_LINES);
        glVertex3f(0.0, 0, 0.0f);
        glVertex3f(0.0, length, 0.0f);

        // arrow
        glVertex3f(0.0, length, 0.0f);
        glVertex3f(0.5, length-0.5, 0.0f);
        glVertex3f(0.0, length, 0.0f);
        glVertex3f(-0.5, length-0.5, 0.0f);
        glEnd();
        glFlush();

        // z
        glColor3f(0.0,0.0,1.0);
        glBegin(GL_LINES);
        glVertex3f(0.0, 0.0f ,0 );
        glVertex3f(0.0, 0.0f ,length );

        // arrow
        glVertex3f(0.0, 0.0f ,length );
        glVertex3f(0.0, 0.5f ,length-0.5 );
        glVertex3f(0.0, 0.0f ,length );
        glVertex3f(0.0, -0.5f ,length-0.5 );
        glEnd();
}
