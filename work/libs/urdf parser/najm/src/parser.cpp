#include <urdf/model.h>
#include <urdf_model/link.h>
#include <urdf_model/types.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include "ros/ros.h"

using namespace std;
using namespace urdf;
void setupRobot();
urdf::Model model;
ofstream myfile;

int main(int argc, char** argv) {
    myfile.open("data.txt");
    string urdf_file = "./katana_400_6m180.urdf";

    if (!model.initFile(urdf_file)) {
        cout << "Failed to parse urdf file\n";
        return -1;
    }

    setupRobot();
    myfile.close();
    return 0;
}

void setupRobot() {
    myfile << "Robot Name: " << model.name_ << '\n';

    //base joint
    myfile << "\n::Base Joint\n";
    JointSharedPtr joint =        model.joints_.find("katana_base_frame_joint")->second;
    myfile << "Name: " << joint->name << endl;
    myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;
    myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    myfile << "Type: " << joint->type << endl;
    myfile << "Movement: base" << endl;
    myfile << "Angle: "<<360 << endl;

    //joint1
    myfile << "\n::Joint1\n";
    joint =        model.joints_.find("katana_motor1_pan_joint")->second;
    myfile << "Name: " << joint->name << endl;
    myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;
    myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    myfile << "Type: " << joint->type << endl;
    myfile << "Movement: joint" << endl;
    myfile << "Angle: "<<180 << endl;

    //joint2
    myfile << "\n::Joint2\n";
    joint =        model.joints_.find("katana_motor3_lift_joint")->second;
    myfile << "Name: " << joint->name << endl;
    myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;
    myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    myfile << "Type: " << joint->type << endl;
    myfile << "Movement: joint" << endl;
    myfile << "Angle: "<<180 << endl;

    //joint3
    myfile << "\n::Joint3\n";
    joint =        model.joints_.find("katana_motor4_lift_joint")->second;
    myfile << "Name: " << joint->name << endl;
    myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;
    myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    myfile << "Type: " << joint->type << endl;
    myfile << "Movement: joint" << endl;
    myfile << "Angle: "<<180 << endl;

    //hand
    myfile << "\n::Hand\n";
    joint =        model.joints_.find("katana_motor5_wrist_roll_joint")->second;
    myfile << "Name: " << joint->name << endl;
    myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;
    myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    myfile << "Type: " << joint->type << endl;
    myfile << "Movement: none" << endl;
    myfile << "Angle: "<<0 << endl;
    
    //link1
    myfile << "\n<<Link1\n";
    LinkSharedPtr link = model.links_.find("katana_base_link")->second;
    myfile << "Name: " << link->name << endl;
    myfile << "Parent_Joint: " << link->parent_joint << endl;
    myfile << "Child_Joint: " << link->child_joints[0]->name << endl;


    //link2
    myfile << "\n<<Link2\n";
    link = model.links_.find("katana_motor2_lift_link")->second;
    myfile << "Name: " << link->name << endl;
    myfile << "Parent_Joint: " << link->parent_joint->name << endl;
    myfile << "Child_Joint: " << link->child_joints[0]->name << endl;

    //link3
    myfile << "\n<<Link3\n";
    link = model.links_.find("katana_motor3_lift_link")->second;
    myfile << "Name: " << link->name << endl;
    myfile << "Parent_Joint: " << link->parent_joint->name << endl;
    myfile << "Child_Joint: " << link->child_joints[0]->name << endl;

    //link4
    myfile << "\n<<Link4\n";
    link = model.links_.find("katana_motor4_lift_link")->second;
    myfile << "Name: " << link->name << endl;
    myfile << "Parent_Joint: " << link->parent_joint->name << endl;
    myfile << "Child_Joint: " << link->child_joints[0]->name << endl;
}

//   enum
//   {
//     UNKNOWN, REVOLUTE, CONTINUOUS, PRISMATIC, FLOATING, PLANAR, FIXED
//   } type;

  /// \brief     type_       meaning of axis_
  /// ------------------------------------------------------
  ///            UNKNOWN     unknown type
  ///            REVOLUTE    rotation axis
  ///            PRISMATIC   translation axis
  ///            FLOATING    N/A
  ///            PLANAR      plane normal axis
  ///            FIXED       N/A

/* model
std::map<std::string, LinkSharedPtr> links_;  //brief complete list of Links
std::map<std::string, JointSharedPtr> joints_; //brief complete list of
Joints std::map<std::string, MaterialSharedPtr> materials_; //brief complete
list of Materials std::string name_; //brief The name of the robot model
LinkSharedPtr root_link_; //brief The root is always a link (the parent of
the tree describing the robot)

// Link
std::string name;
InertialSharedPtr inertial;
VisualSharedPtr visual;
CollisionSharedPtr collision;
std::vector<CollisionSharedPtr> collision_array;
std::vector<VisualSharedPtr> visual_array;
JointSharedPtr parent_joint;
std::vector<JointSharedPtr> child_joints;
std::vector<LinkSharedPtr> child_links;

// Joint
std::string name;
enum{
  UNKNOWN, REVOLUTE, CONTINUOUS, PRISMATIC, FLOATING, PLANAR, FIXED
} type;
Vector3 axis;
std::string child_link_name;
std::string parent_link_name;
Pose  parent_to_joint_origin_transform;
JointDynamicsSharedPtr dynamics;
JointLimitsSharedPtr limits;
JointSafetySharedPtr safety;
JointCalibrationSharedPtr calibration;
JointMimicSharedPtr mimic;*/

// //Basic Info
//     myfile << "Successfully parsed urdf file" << '\n';
//     myfile << "Robot Name: " << model.name_ << '\n';
//     myfile << "\nRoot Link...." << endl;
//     myfile << model.getRoot()->name << endl;

//     //All Robot Links
//     myfile << "\nPrinting All Links...." << endl;
//     int i = 1;
//     map<string, LinkSharedPtr>::iterator itr;
//     for (itr = model.links_.begin(); itr != model.links_.end(); ++itr) {
//         myfile << i << ":  " << itr->first << '\t' << itr->second << '\n';
    //     i++;
    // }

    // //All Robot Joints
    // myfile << "\nPrinting All Joints...." << endl;
    // i = 1;
    // map<string, JointSharedPtr>::iterator itr2;
    // for (itr2 = model.joints_.begin(); itr2 != model.joints_.end(); ++itr2) {
    //     myfile << i << ":  " << itr2->first << '\t' << itr2->second << '\n';
    //     i++;
    // }

    // //Single Link Information
    // myfile << "\nSome information on base link....\n";
    // LinkSharedPtr link = model.links_.find("katana_base_link")->second;
    // myfile << "Name: " << link->name << endl;
    // myfile << "Inertial: " << link->inertial->mass << endl;
    // myfile << "Parent Joint: " << link->parent_joint << endl;
    // myfile << "Child Joints: " << link->child_joints[0]->name << endl;
    // myfile << "Child Links: " << link->child_links[0]->name << endl;

    // //Single Joint Information
    // myfile << "\nSome information on katana_base_frame_joint....\n";
    // JointSharedPtr joint =        model.joints_.find("katana_base_frame_joint")->second;
    // myfile << "Name: " << joint->name << endl;
    // myfile << "Type: " << joint->type << endl;
    // myfile << "Child_Link_Name: " << joint->child_link_name << endl;
    // myfile << "Parent_Link_Name: " << joint->parent_link_name << endl;

    // //Creating Tree Information
    // std::map<std::string, std::string> tree;
    // model.initTree(tree);

    // myfile << "\nParent Link Tree...." << endl;
    // myfile << "Child\t\t\t\tParent" << endl;
    // i = 1;
    // map<string, string>::iterator itr3;
    // for (itr3 = tree.begin(); itr3 != tree.end(); ++itr3) {
    //     myfile << i << ":  " << itr3->first << "          " << itr3->second << '\n';
    //     i++;
    // }